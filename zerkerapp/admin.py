from django.contrib import admin

# Register your models here.
from .models import DonationItem
from .models import OnlinePlayers
from .models import HsUsers
from .models import Players
from .models import Itemcategories

admin.site.register(DonationItem)
admin.site.register(OnlinePlayers)
admin.site.register(HsUsers)
admin.site.register(Players)
admin.site.register(Itemcategories)