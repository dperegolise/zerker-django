
$( document ).ready(function() {
    points = 0;
    output = 0;
    minlevel = 2; // first level to display
    maxlevel = 200; // last level to display
    pointsArr = [];
    pointsArr[0]= -1;
    pointsArr[1]= 0;

    //create lvl to xp arr
    for (lvl = 1; lvl <= maxlevel; lvl++)
    {
        points += Math.floor(lvl + 300 * Math.pow(2, lvl / 7.));    
        pointsArr[lvl]=output;
        output = Math.floor(points / 4);
    }

    //set lvl values in html
    $( "#highscores_table tr" ).each(function( index ) {
        if(index !== 0) {
            var xp = $(this).find(".xp_td").html(); 
            level = findLvl(xp);
            $(this).find(".lvl_td").text(level);
            console.log(xp + " "+ level);
        }   
    });

    function findLvl(xp) {
        index = binaryIndexOf(pointsArr, xp);
        return index;
    }

    //search xp to lvl arr using binary search
    function binaryIndexOf(arr, searchElement) {
        'use strict';

        var minIndex = 0;
        var maxIndex = arr.length - 1;
        var currentIndex;
        var currentElement;
        var previousElement;

        while (minIndex <= maxIndex) {
            currentIndex = (minIndex + maxIndex) / 2 | 0;
            currentElement = arr[currentIndex];
            previousElement = arr[currentIndex-1];
            console.log('previous: '+previousElement+' current: '+currentElement+' search: '+searchElement);
            if( searchElement > previousElement && searchElement < currentElement  ) {
                return currentIndex-1;
            }
            else if (currentElement == searchElement) {
                return currentIndex;
            }
            else if (currentElement < searchElement ) {
                minIndex = currentIndex + 1;
                previousElement = currentElement;
            }
            else if (currentElement > searchElement) {
                maxIndex = currentIndex - 1;
                previousElement = currentElement;
            }
        }

        return -1;
    }


});





