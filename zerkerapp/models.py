from django.db import models

class DonationItem(models.Model):
    product_id = models.IntegerField()
    item_name = models.CharField(max_length=30)
    item_description = models.CharField(max_length=30)
    price = models.FloatField(default=5.99)
    cat_id = models.IntegerField(default=5)

class Itemcategories(models.Model):
	cat_name = models.CharField(max_length=45, blank=True, null=True)


class HsUsers(models.Model):
    username = models.CharField(max_length=40, blank=True, null=True)
    rights = models.IntegerField(blank=True, null=True)
    overall_xp = models.BigIntegerField(blank=True, null=True)
    attack_xp = models.IntegerField(blank=True, null=True)
    defence_xp = models.IntegerField(blank=True, null=True)
    strength_xp = models.IntegerField(blank=True, null=True)
    constitution_xp = models.IntegerField(blank=True, null=True)
    ranged_xp = models.IntegerField(blank=True, null=True)
    prayer_xp = models.IntegerField(blank=True, null=True)
    magic_xp = models.IntegerField(blank=True, null=True)
    cooking_xp = models.IntegerField(blank=True, null=True)
    woodcutting_xp = models.IntegerField(blank=True, null=True)
    fletching_xp = models.IntegerField(blank=True, null=True)
    fishing_xp = models.IntegerField(blank=True, null=True)
    firemaking_xp = models.IntegerField(blank=True, null=True)
    crafting_xp = models.IntegerField(blank=True, null=True)
    smithing_xp = models.IntegerField(blank=True, null=True)
    mining_xp = models.IntegerField(blank=True, null=True)
    herblore_xp = models.IntegerField(blank=True, null=True)
    agility_xp = models.IntegerField(blank=True, null=True)
    thieving_xp = models.IntegerField(blank=True, null=True)
    slayer_xp = models.IntegerField(blank=True, null=True)
    farming_xp = models.IntegerField(blank=True, null=True)
    runecrafting_xp = models.IntegerField(blank=True, null=True)
    hunter_xp = models.IntegerField(blank=True, null=True)
    construction_xp = models.IntegerField(blank=True, null=True)
    summoning_xp = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'hs_users'


class Players(models.Model):
    username = models.CharField(max_length=45, blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    rights = models.CharField(max_length=45, blank=True, null=True)
    ip = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'players'

class OnlinePlayers(models.Model):
    online = models.IntegerField()
    max_online = models.IntegerField()
    
    class Meta:
        managed = False
        db_table = 'onlineplayers'


