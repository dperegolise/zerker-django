from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import resolve

from .models import OnlinePlayers
from .models import DonationItem
from .models import Itemcategories
from .models import HsUsers
from .zerker_forum_models import Posts

def webclient(request):
    return render(request, 'zerkerapp/webclient.html')

def index(request):
    forum_posts = Posts.objects.raw('SELECT posts.*, topics.subject FROM zerker_forum.posts posts join zerker_forum.topics topics on posts.topic_id=topics.id;')[:5];
    context = {'online': OnlinePlayers.objects.all(), 'highscores': HsUsers.objects.order_by('-overall_xp').all()[:5], 'forum_posts': forum_posts }
    return render(request, 'zerkerapp/index.html', context=context)

def vote(request):
    return render(request, 'zerkerapp/vote.html')


def donateArmor(request):
    donation_items = DonationItem.objects.filter(cat_id__exact=1)
    context = getDonateContext(request, donation_items)
    
    return render(request, 'zerkerapp/donate.html', context=context)

def donateMisc(request):
    donation_items = DonationItem.objects.filter(cat_id__exact=5)
    context = getDonateContext(request, donation_items)
    
    return render(request, 'zerkerapp/donate.html', context=context)

def donateFood(request):
    donation_items = DonationItem.objects.filter(cat_id__exact=2)
    context = getDonateContext(request, donation_items)
    
    return render(request, 'zerkerapp/donate.html', context=context)

def donateWeapons(request):
    donation_items = DonationItem.objects.filter(cat_id__exact=3)
    context = getDonateContext(request, donation_items)
    
    return render(request, 'zerkerapp/donate.html', context=context)

def donateRank(request):
    donation_items = DonationItem.objects.filter(cat_id__exact=4)
    context = getDonateContext(request, donation_items)
    
    return render(request, 'zerkerapp/donate.html', context=context)

def getDonateContext(request, donationItems):
    paginator = Paginator(donationItems, 5)
    current_url = resolve(request.path_info).url_name[6:]
    page = request.GET.get('page')

    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        items = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        items = paginator.page(paginator.num_pages)

    context = {'itemcats': Itemcategories.objects.all(), 'items': items, 'pageName': current_url}
    return context;