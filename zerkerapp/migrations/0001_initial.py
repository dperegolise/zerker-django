# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HsUsers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=40, null=True, blank=True)),
                ('rights', models.IntegerField(null=True, blank=True)),
                ('overall_xp', models.BigIntegerField(null=True, blank=True)),
                ('attack_xp', models.IntegerField(null=True, blank=True)),
                ('defence_xp', models.IntegerField(null=True, blank=True)),
                ('strength_xp', models.IntegerField(null=True, blank=True)),
                ('constitution_xp', models.IntegerField(null=True, blank=True)),
                ('ranged_xp', models.IntegerField(null=True, blank=True)),
                ('prayer_xp', models.IntegerField(null=True, blank=True)),
                ('magic_xp', models.IntegerField(null=True, blank=True)),
                ('cooking_xp', models.IntegerField(null=True, blank=True)),
                ('woodcutting_xp', models.IntegerField(null=True, blank=True)),
                ('fletching_xp', models.IntegerField(null=True, blank=True)),
                ('fishing_xp', models.IntegerField(null=True, blank=True)),
                ('firemaking_xp', models.IntegerField(null=True, blank=True)),
                ('crafting_xp', models.IntegerField(null=True, blank=True)),
                ('smithing_xp', models.IntegerField(null=True, blank=True)),
                ('mining_xp', models.IntegerField(null=True, blank=True)),
                ('herblore_xp', models.IntegerField(null=True, blank=True)),
                ('agility_xp', models.IntegerField(null=True, blank=True)),
                ('thieving_xp', models.IntegerField(null=True, blank=True)),
                ('slayer_xp', models.IntegerField(null=True, blank=True)),
                ('farming_xp', models.IntegerField(null=True, blank=True)),
                ('runecrafting_xp', models.IntegerField(null=True, blank=True)),
                ('hunter_xp', models.IntegerField(null=True, blank=True)),
                ('construction_xp', models.IntegerField(null=True, blank=True)),
                ('summoning_xp', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'db_table': 'hs_users',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Players',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=45, null=True, blank=True)),
                ('password', models.CharField(max_length=255, null=True, blank=True)),
                ('rights', models.CharField(max_length=45, null=True, blank=True)),
                ('ip', models.CharField(max_length=45, null=True, blank=True)),
            ],
            options={
                'db_table': 'players',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DonationItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_id', models.IntegerField()),
                ('item_name', models.CharField(max_length=30)),
                ('item_description', models.CharField(max_length=30)),
                ('price', models.FloatField(default=5.99)),
                ('cat_id', models.IntegerField(default=5)),
            ],
        ),
        migrations.CreateModel(
            name='OnlinePlayers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('online', models.IntegerField()),
                ('max_online', models.IntegerField()),
            ],
        ),
    ]
