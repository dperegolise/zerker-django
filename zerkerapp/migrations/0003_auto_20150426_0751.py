# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zerkerapp', '0002_auto_20150426_0549'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='onlineplayers',
            table='onlineplayers',
        ),
    ]
