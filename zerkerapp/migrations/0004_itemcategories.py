# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('zerkerapp', '0003_auto_20150426_0751'),
    ]

    operations = [
        migrations.CreateModel(
            name='Itemcategories',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cat_name', models.CharField(max_length=45, null=True, blank=True)),
            ],
        ),
    ]
