from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'donate/(?:a|A)rmor$', views.donateArmor, name='donateArmor'),
    url(r'donate$', views.donateArmor, name='donateArmor'),
    url(r'donate/(?:f|F)ood$', views.donateFood, name='donateFood'),
    url(r'donate/(?:w|W)eapons$', views.donateWeapons, name='donateWeapons'),
    url(r'donate/(?:R|R)ank$', views.donateRank, name='donateRank'),
    url(r'donate/(?:m|M)isc$', views.donateMisc, name='donateMisc'),
    url(r'vote$', views.vote, name='vote'),
    url(r'webclient$', views.webclient, name='webclient'),
]