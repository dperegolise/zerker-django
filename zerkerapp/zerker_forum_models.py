# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup)
    permission = models.ForeignKey('AuthPermission')

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group_id', 'permission_id'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    content_type = models.ForeignKey('DjangoContentType')
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type_id', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254, blank=True, null=True)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser)
    group = models.ForeignKey(AuthGroup)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user_id', 'group_id'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser)
    permission = models.ForeignKey(AuthPermission)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user_id', 'permission_id'),)


class Bans(models.Model):
    username = models.CharField(max_length=200, blank=True, null=True)
    ip = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=80, blank=True, null=True)
    message = models.CharField(max_length=255, blank=True, null=True)
    expire = models.IntegerField(blank=True, null=True)
    ban_creator = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'bans'


class Categories(models.Model):
    cat_name = models.CharField(max_length=80)
    disp_position = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'categories'


class Censoring(models.Model):
    search_for = models.CharField(max_length=60)
    replace_with = models.CharField(max_length=60)

    class Meta:
        managed = False
        db_table = 'censoring'


class Config(models.Model):
    conf_name = models.CharField(primary_key=True, max_length=255)
    conf_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'config'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', blank=True, null=True)
    user = models.ForeignKey(AuthUser)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class ForumPerms(models.Model):
    group_id = models.IntegerField()
    forum_id = models.IntegerField()
    read_forum = models.IntegerField()
    post_replies = models.IntegerField()
    post_topics = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'forum_perms'
        unique_together = (('group_id', 'forum_id'),)


class ForumSubscriptions(models.Model):
    user_id = models.IntegerField()
    forum_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'forum_subscriptions'
        unique_together = (('user_id', 'forum_id'),)


class Forums(models.Model):
    forum_name = models.CharField(max_length=80)
    forum_desc = models.TextField(blank=True, null=True)
    redirect_url = models.CharField(max_length=100, blank=True, null=True)
    moderators = models.TextField(blank=True, null=True)
    num_topics = models.IntegerField()
    num_posts = models.IntegerField()
    last_post = models.IntegerField(blank=True, null=True)
    last_post_id = models.IntegerField(blank=True, null=True)
    last_poster = models.CharField(max_length=200, blank=True, null=True)
    sort_by = models.IntegerField()
    disp_position = models.IntegerField()
    cat_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'forums'


class Groups(models.Model):
    g_id = models.AutoField(primary_key=True)
    g_title = models.CharField(max_length=50)
    g_user_title = models.CharField(max_length=50, blank=True, null=True)
    g_promote_min_posts = models.IntegerField()
    g_promote_next_group = models.IntegerField()
    g_moderator = models.IntegerField()
    g_mod_edit_users = models.IntegerField()
    g_mod_rename_users = models.IntegerField()
    g_mod_change_passwords = models.IntegerField()
    g_mod_ban_users = models.IntegerField()
    g_mod_promote_users = models.IntegerField()
    g_read_board = models.IntegerField()
    g_view_users = models.IntegerField()
    g_post_replies = models.IntegerField()
    g_post_topics = models.IntegerField()
    g_edit_posts = models.IntegerField()
    g_delete_posts = models.IntegerField()
    g_delete_topics = models.IntegerField()
    g_post_links = models.IntegerField()
    g_set_title = models.IntegerField()
    g_search = models.IntegerField()
    g_search_users = models.IntegerField()
    g_send_email = models.IntegerField()
    g_post_flood = models.SmallIntegerField()
    g_search_flood = models.SmallIntegerField()
    g_email_flood = models.SmallIntegerField()
    g_report_flood = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = 'groups'


class Online(models.Model):
    user_id = models.IntegerField()
    ident = models.CharField(max_length=200)
    logged = models.IntegerField()
    idle = models.IntegerField()
    last_post = models.IntegerField(blank=True, null=True)
    last_search = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'online'
        unique_together = (('user_id', 'ident'),)


class Posts(models.Model):
    poster = models.CharField(max_length=200)
    poster_id = models.IntegerField()
    poster_ip = models.CharField(max_length=39, blank=True, null=True)
    poster_email = models.CharField(max_length=80, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    hide_smilies = models.IntegerField()
    posted = models.IntegerField()
    edited = models.IntegerField(blank=True, null=True)
    edited_by = models.CharField(max_length=200, blank=True, null=True)
    topic_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'posts'


class Reports(models.Model):
    post_id = models.IntegerField()
    topic_id = models.IntegerField()
    forum_id = models.IntegerField()
    reported_by = models.IntegerField()
    created = models.IntegerField()
    message = models.TextField(blank=True, null=True)
    zapped = models.IntegerField(blank=True, null=True)
    zapped_by = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'reports'


class SearchCache(models.Model):
    id = models.IntegerField(primary_key=True)
    ident = models.CharField(max_length=200)
    search_data = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'search_cache'


class SearchMatches(models.Model):
    post_id = models.IntegerField()
    word_id = models.IntegerField()
    subject_match = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'search_matches'


class SearchWords(models.Model):
    id = models.AutoField()
    word = models.CharField(primary_key=True, max_length=20)

    class Meta:
        managed = False
        db_table = 'search_words'


class TopicSubscriptions(models.Model):
    user_id = models.IntegerField()
    topic_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'topic_subscriptions'
        unique_together = (('user_id', 'topic_id'),)


class Topics(models.Model):
    id = models.CharField(max_length=200)
    poster = models.CharField(max_length=200)
    subject = models.CharField(max_length=255)
    posted = models.IntegerField()
    first_post_id = models.IntegerField()
    last_post = models.IntegerField()
    last_post_id = models.IntegerField()
    last_poster = models.CharField(max_length=200, blank=True, null=True)
    num_views = models.IntegerField()
    num_replies = models.IntegerField()
    closed = models.IntegerField()
    sticky = models.IntegerField()
    moved_to = models.IntegerField(blank=True, null=True)
    forum_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'topics'


class Users(models.Model):
    group_id = models.IntegerField()
    username = models.CharField(unique=True, max_length=200)
    password = models.CharField(max_length=40)
    email = models.CharField(max_length=80)
    title = models.CharField(max_length=50, blank=True, null=True)
    realname = models.CharField(max_length=40, blank=True, null=True)
    url = models.CharField(max_length=100, blank=True, null=True)
    jabber = models.CharField(max_length=80, blank=True, null=True)
    icq = models.CharField(max_length=12, blank=True, null=True)
    msn = models.CharField(max_length=80, blank=True, null=True)
    aim = models.CharField(max_length=30, blank=True, null=True)
    yahoo = models.CharField(max_length=30, blank=True, null=True)
    location = models.CharField(max_length=30, blank=True, null=True)
    signature = models.TextField(blank=True, null=True)
    disp_topics = models.IntegerField(blank=True, null=True)
    disp_posts = models.IntegerField(blank=True, null=True)
    email_setting = models.IntegerField()
    notify_with_post = models.IntegerField()
    auto_notify = models.IntegerField()
    show_smilies = models.IntegerField()
    show_img = models.IntegerField()
    show_img_sig = models.IntegerField()
    show_avatars = models.IntegerField()
    show_sig = models.IntegerField()
    timezone = models.FloatField()
    dst = models.IntegerField()
    time_format = models.IntegerField()
    date_format = models.IntegerField()
    language = models.CharField(max_length=25)
    style = models.CharField(max_length=25)
    num_posts = models.IntegerField()
    last_post = models.IntegerField(blank=True, null=True)
    last_search = models.IntegerField(blank=True, null=True)
    last_email_sent = models.IntegerField(blank=True, null=True)
    last_report_sent = models.IntegerField(blank=True, null=True)
    registered = models.IntegerField()
    registration_ip = models.CharField(max_length=39)
    last_visit = models.IntegerField()
    admin_note = models.CharField(max_length=30, blank=True, null=True)
    activate_string = models.CharField(max_length=80, blank=True, null=True)
    activate_key = models.CharField(max_length=8, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
