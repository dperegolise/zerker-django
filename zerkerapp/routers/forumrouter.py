
forumgroup = ['Posts']

class ForumRouter(object):
    """
    A router to control all database operations on models in the
    auth application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read auth models go to auth_db.
        """
        for djangomodel in forumgroup: 
            if (model.__name__ == djangomodel):
                return 'zerker_forum'

        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models go to auth_db.
        """
        for djangomodel in forumgroup: 
            if (model.__name__ == djangomodel):
                return 'zerker_forum'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth app is involved.
        """
        for djangomodel in forumgroup: 
            if (obj1.__name__ == djangomodel or obj2.__name__ == djangomodel):
                return 'zerker_forum'
        return None

    def allow_migrate(self, db, app_label, model, **hints):
        """
        Make sure the auth app only appears in the 'auth_db'
        database.
        """
        if model.__name__ == 'Posts':
            return db == 'default'
        return None