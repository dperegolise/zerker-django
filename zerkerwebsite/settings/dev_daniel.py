from .base import *

SECRET_KEY = 'fo1y(z2ki$b^f=y^fgrjm3s5*hkybg6vaa^!bvc81&m9!q8rmw'

DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'django_test',
        'USER': 'root',
        'PASSWORD': 'toor',
        'HOST': '127.0.0.1',
        'PORT': '3306'
    },
    'zerker_server': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'zerker_server',
        'USER': 'root',
        'PASSWORD': 'toor',
        'HOST': '127.0.0.1',
        'PORT': '3306'
    },
    'zerker_forum': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'zerker_forum',
        'USER': 'root',
        'PASSWORD': 'toor',
        'HOST': '127.0.0.1',
        'PORT': '3306'
    }
}
